renderdoc-traces
================

This repository is dead. See:

- [traces-db](https://gitlab.freedesktop.org/gfx-ci/tracie/traces-db/) for repo of traces
- [gpu-trace-perf](https://crates.io/crates/gpu-trace-perf) for a runner for
  both apitraces and renderdoc.
