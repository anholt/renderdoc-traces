#!/usr/bin/env python3

import re
import shlex
import sys
import os
import time
import argparse
import subprocess
import numpy as np
from math import sqrt
from scipy import stats

class Stats:
    def __init__(self, alpha, x, y):
        meanx = np.mean(x)
        meany = np.mean(y)
        nx = len(x)
        ny = len(y)
        assert(nx > 1)
        assert(ny > 1)

        df = nx + ny - 2
        varx = np.var(x, ddof=1)
        vary = np.var(y, ddof=1)

        # cohen's pooled variance
        pooled_sd = sqrt(((nx-1) * varx + (ny-1) * vary) / df)
        # standard error
        se = pooled_sd * sqrt(1.0 / nx + 1.0 / ny)

        # We're doing two-tailed (you might increase or decrease perf), so 95%
        # CI uses the .975 value.
        t = stats.t.ppf(1.0 - (alpha / 2), df)
        e = t * se

        self.percent_change = (meany - meanx) / meanx * 100
        self.percent_error = e / meanx * 100

def test_stats():
    import pytest

    before = [1, 2, 3, 4, 5]
    after = [5, 6, 7, 8, 9]

    stats = Stats(.05, before, after)

    # Values here came from ministat output, with a bit of tweaking because
    # their t table is lower precision than scipy.
    assert stats.percent_change == pytest.approx(133.333333)
    assert stats.percent_error == pytest.approx(76.8668)

def process_directories(items):
    for item in items:
        if os.path.isfile(item):
            yield item
        else:
            for dirpath, _, filenames in os.walk(item):
                for fname in filenames:
                    if os.path.splitext(fname)[1] == '.rdc':
                        yield os.path.join(dirpath, fname)


def run_capture(env, remote, wrapper, filename, framecount):
    if remote:
        filename = shlex.quote(filename)
    replay_cmd = [wrapper, 'renderdoccmd', 'replay', filename, '-l', str(framecount)]

    if remote:
        (host, dir) = remote.split(':')
        for var in env:
            replay_cmd = ['{}={}'.format(var, env[var])] + replay_cmd
        replay_cmd = ['cd', shlex.quote(dir), '&&'] + replay_cmd
        replay_cmd = ['ssh', host, ' '.join(replay_cmd)]

    try:
        p = subprocess.Popen(
            replay_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        (stdout, stderr) = p.communicate()
        results = (stdout + stderr).decode("utf-8")
    except KeyboardInterrupt:
        exit(1)
    #except:
    #    return filename + " FAIL\n"

    fps = []

    lines = (line for line in results.splitlines())
    re_fps = re.compile(r'FPS: (.*)')
    re_preload = re.compile(r'.*from LD_PRELOAD cannot be preloaded.*')
    for line in lines:
        if re_preload.match(line):
            raise Exception(line)

        fps_match = re_fps.match(line)
        if fps_match:
            fps.append(float(fps_match.group(1)))

    if not fps:
        print('Failed to collect any FPS results from command: ', replay_cmd)
        print('Output:')
        print(results)
        exit(1)

    return fps

def print_stats(before, after):
    change = []
    for filename in before:
        mean_before = np.mean(before[filename])
        mean_after = np.mean(after[filename])
        delta = (mean_after - mean_before) / mean_before
        change.append((delta, filename))

    change.sort(reverse=True)

    namelen = max(len(filename) for (delta, filename) in change)
    for (delta, filename) in change:
        # Apply the Bonferroni correction for multiple hypothesis testing.
        stats = Stats(.05 / len(before), before[filename], after[filename])

        print("{filename:{namelen}} {percentage:+5.1f}% +/- {error:5.1f}% ".format(
            percentage=stats.percent_change,
            filename=filename + ":",
            namelen=namelen + 1,
            error=stats.percent_error))

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("before",
                        help="Wrapper command for setting the environment for "
                             "the 'before' case")
    parser.add_argument("after",
                        help="Wrapper command for setting the environment for "
                             "the 'after' case")
    parser.add_argument("--frames",
                        type=int,
                        default=0,
                        help="Total number of frames to capture per trace (0 for indefinite)")
    # Group size lets us reduce the impact of thermal throttling (and other
    # long-term intermittent behavior) on the independence of our samples, by
    # letting us flip-flop between before and after states during capture.
    parser.add_argument("--groupsize",
                        type=int,
                        default=5,
                        help="Number of frames to capture per renderdoc invocation (0 for indefinite)")
    parser.add_argument("--remote",
                        help="The host and directory (mirroring the local renderdoc-traces/) to run renderdoccmd on. "
                        "(e.g. remote-device:renderdoc-traces)")
    parser.add_argument("traces",
                        nargs='*',
                        default=['traces'],
                        metavar="<rdc_file | traces dir>",
                        help="A directory containing renderdoc traces. "
                             "Defaults to 'traces/'")
    parser.add_argument("--preload",
                        action="store_true",
                        help="Use a LD_PRELOAD shim for replaying with a "
                             "stock renderdoc (instead of the fps-hacks branch")

    args = parser.parse_args()

    env = {}
    env["vblank_mode"] = "0"
    env["allow_glsl_extension_directive_midshader"] = "true"

    # Each replay of the frame involves setting up the whole state again,
    # including linking.  Avoid expensive validation on debug builds.
    env["NIR_VALIDATE"] = "0"

    if args.preload:
        env["LD_PRELOAD"] = "librenderdoc_replay_shim.so"

    if not args.remote:
        for key in env:
            os.environ[key] = env[key]

    filenames = []
    for filename in process_directories(args.traces):
        filenames.append(filename)
    filenames.sort()

    before = {}
    after = {}

    warm = False

    frames_collected = 0
    before_first = True
    while args.frames == 0 or frames_collected < args.frames:
        run_frame_count = args.groupsize
        if args.frames != 0:
            run_frame_count = min(args.groupsize, args.frames - frames_collected)
        for filename in filenames:
            if not warm:
                print("Warming up the GPU with {}".format(filename))
                run_capture(env, args.remote, args.before, filename, 60)
                warm = True

            print("Running {}".format(filename))

            if filename not in before:
                before[filename] = []
                after[filename] = []

            if before_first:
                before[filename] += run_capture(env, args.remote, args.before, filename, run_frame_count)
                after[filename] += run_capture(env, args.remote, args.after, filename, run_frame_count)
            else:
                after[filename] += run_capture(env, args.remote, args.after, filename, run_frame_count)
                before[filename] += run_capture(env, args.remote, args.before, filename, run_frame_count)

        before_first = not before_first
        frames_collected += run_frame_count

        # Print stats after each group, whether or not we're in indefinite
        # mode.  This will be useful for quickly seeing that your change
        # is/isn't working.
        print('')
        print('Results: ({collecting}/{target} frames)'.format(
            collecting = frames_collected + run_frame_count,
            target = args.frames if args.frames != 0 else '∞'))
        print_stats(before, after)
        if frames_collected != args.frames:
            print('')

if __name__ == "__main__":
    main()
