/* Copyright © 2013, Intel Corporation
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#define GL_GLEXT_PROTOTYPES
#include <GLES2/gl2.h>
#include <stdio.h>
#include <stdlib.h>

#include "dlwrap.h"
#include "glwrap.h"

static void *gl_handle;

void
glwrap_set_gl_handle (void *handle)
{
    if (gl_handle == NULL)
        gl_handle = handle;
}

void *
glwrap_lookup (char *name)
{
    void *ret;

    /* We don't call dlopen here to find the library in which to
     * perform a dlsym lookup. That's because the application may
     * be loading libGL.so or libGLESv2.so for its OpenGL symbols.
     *
     * So we instead watch for one of those filenames to go by in
     * our dlopen wrapper, which will then call
     * glwrap_set_gl_handle to give us the handle to use here.
     *
     * If the application hasn't called dlopen on a "libGL"
     * library, then presumably the application is linked directly
     * to an OpenGL implementation. In this case, we can use
     * RTLD_NEXT to find the symbol.
     */
    if (gl_handle == NULL)
        gl_handle = RTLD_NEXT;

    ret = dlwrap_real_dlsym (gl_handle, name);

    if (ret == NULL) {
        fprintf (stderr, "fips: Error: glwrap_lookup failed to dlsym %s\n",
                 name);
        exit (1);
    }

    return ret;
}
