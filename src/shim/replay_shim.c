/*
 * Copyright © 2019 Google LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice (including the next
 * paragraph) shall be included in all copies or substantial portions of the
 * Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "replay_shim.h"
#include "glwrap.h"

static bool frame_skip = false;
static int frame_start_count = 0;
static int frame_end_count = 0;
static int64_t frame_start_time;

static void (*real_glFinish)(void);

/* Cut out the usleep loop in renderdoccmd replay */
int usleep(useconds_t usec)
{
    return 0;
}

int64_t get_time_ns(void)
{
   struct timespec tv;
   clock_gettime(CLOCK_MONOTONIC, &tv);
   return tv.tv_nsec + tv.tv_sec * 1000000000ll;
}

void
frame_start(void)
{
    assert(frame_start_count == frame_end_count);
    frame_start_count++;

    if (frame_start_count == 1) {
        real_glFinish = glwrap_lookup("glFinish");
    }

    real_glFinish();

    frame_start_time = get_time_ns();
}

void
frame_end(void)
{
    if (frame_skip) {
        frame_skip = false;
        return;
    }

    frame_end_count++;
    assert(frame_end_count == frame_start_count);

    real_glFinish();

    int64_t frame_end_time = get_time_ns();
    int64_t duration = frame_end_time - frame_start_time;

    fprintf(stderr, "FPS: %f\n", 1000000000.0 / duration);
}


void
glDebugMessageInsert(GLenum source, GLenum type, GLuint id, GLenum severity,
                     GLsizei length, const GLchar *buf)
{
    const char *skip_msg = "!!!!RenderDoc Internal:  Replay 2";
    const char *start_msg = "!!!!RenderDoc Internal:  Replay 1";
    const char *end_msg = "!!!!RenderDoc Internal: Done replay";

    if (strncmp(buf, skip_msg, strlen(skip_msg)) == 0)
        frame_skip = true;
    else if (strncmp(buf, start_msg, strlen(start_msg)) == 0)
        frame_start();
    else if (strncmp(buf, end_msg, strlen(end_msg)) == 0)
        frame_end();
}
